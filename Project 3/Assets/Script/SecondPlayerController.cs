﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondPlayerController : Controller {

    public Transform tf;
    public float speed;
    public float turnspeed;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        //move forward
        if (Input.GetKey(KeyCode.UpArrow))
        {
            tf.position = tf.position + tf.right;
            //tf.Rotate (0,0,turnspeed);
        }
        //rotate left
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            //tf.position = tf.position + tf.up;
            tf.Rotate(0, 0, turnspeed);
        }
        //rotate right
        if (Input.GetKey(KeyCode.RightArrow))
        {
            // tf.position = tf.position - tf.up;
            tf.Rotate(0, 0, -turnspeed);
        }
        //move backwards
        if (Input.GetKey(KeyCode.DownArrow))
        {
            // tf.position = tf.position - tf.right;
            //tf.Rotate (0,0,turnspeed);
        }
    }
}
