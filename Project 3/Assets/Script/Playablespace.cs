﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playablespace : MonoBehaviour {
	public void OnTriggerExit2D (Collider2D otherCollider) {
		//if they leave the area destroy the gameObject
		Destroy (otherCollider.gameObject);
	}
}
