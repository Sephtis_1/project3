﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour {
	//tell c# that all pawns will shout and the will do it the same way
	public void Shout(){
		Debug.Log ("Shout!");
	}

	//tell c# that akk pawns have this function, but, we expect to overide it in the child pawns
	public virtual void Move (Vector3 direction){
	
	}

}
