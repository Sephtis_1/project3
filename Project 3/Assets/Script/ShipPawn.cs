﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ShipPawn : Pawn {

    //ovverides the pawn parent move to make the pawn subclass follow the following direction
	public override void Move (Vector3 direction){
		GetComponent<Transform> ().position += direction;
	}
}
