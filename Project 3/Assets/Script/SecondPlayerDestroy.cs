﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondPlayerDestroy : MonoBehaviour {

    private SpriteRenderer sr;
    int targetvalue;
    int life;
    public Transform tf;

    // Use this for initialization
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        targetvalue = 0;
        life = 5;
        tf = GetComponent<Transform>();

    }

    // Update is called once per frame
    void Update()
    {

    }
    //on collision the player ship resets at it's start point and loses a life
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (life > targetvalue)
        {
            Vector3 startPos = default(Vector3);
            tf.position = startPos;
            --life;
        }
        //destroys the player ship if they run out of lives
        if (life == targetvalue)
        {
            Destroy(gameObject);
        }
    }
    void OnCollisionEnter2D()
    {
        Debug.Log("You have been DESTROYED");
    }
}
